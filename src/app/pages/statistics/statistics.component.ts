import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {Label} from "ng2-charts";
import {ChartDataSets} from "chart.js";

@Component({
    selector: 'app-statistics',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit, AfterViewInit {
    public height?: string;
    public lineChartData: ChartDataSets[] = [
        {data: [500, 250, 100, 400, 70, 150, 420, 480], label: 'Loose'},
        {data: [220, 380, 80, 135, 250, 280, 120, 150], label: 'ULD'},
    ];
    public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August'];
    @ViewChild('chartWrap')
    public chartWrap?: ElementRef<HTMLDivElement>;

    constructor() {
    }

    public ngOnInit(): void {
    }

    public ngAfterViewInit(): void {
        setTimeout(this.onResize.bind(this), 1);
    }

    @HostListener('window:resize')
    public onResize() {
        this.height = undefined;
        setTimeout(() => {
            const height = this.chartWrap?.nativeElement.clientHeight;
            this.height = "" + (height && height < 300 ? 300 : height) + "px";
        }, 1);
    }
}
