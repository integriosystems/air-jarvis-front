import {Component, OnInit} from '@angular/core';
import {ChartOptions} from "chart.js";

const chart2Labels = ['Custom cancellations', 'Weather conditions', 'Missed flights'];

@Component({
    selector: 'app-cancellations',
    templateUrl: './cancellations.component.html',
    styleUrls: ['./cancellations.component.scss']
})
export class CancellationsComponent implements OnInit {

    public chart1Data = [8, 17, 29, 12, 21, 15, 11];
    public chart1Labels = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];
    public chart2Data = [26, 54, 30];
    public chart2Labels = chart2Labels;
    public chart2Options: ChartOptions = {
        legend: {position: "bottom", labels: {usePointStyle: true}},
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    return chart2Labels[tooltipItem.index!] + ': ' + data.datasets![tooltipItem.datasetIndex!].data![tooltipItem.index!] + "%";
                }
            }
        }
    }

    constructor() {
    }

    ngOnInit(): void {
    }

}
