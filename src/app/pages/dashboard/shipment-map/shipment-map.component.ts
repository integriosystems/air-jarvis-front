import {ChangeDetectorRef, Component, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Station} from "../../../shared/model/station.model";
import {LiveFlight} from "../../../shared/model/live-flight.model";
import {AgmInfoWindow} from '@agm/core';
import {Booking} from '../../../shared/model/booking.model';

@Component({
    selector: 'app-shipment-map',
    templateUrl: './shipment-map.component.html',
    styleUrls: ['./shipment-map.component.scss']
})
export class ShipmentMapComponent implements OnInit {
    public stations: Station[] = [];
    public flights: LiveFlight[] = [];

    public flightMarkerBookings?: Booking[];
    public previousMarkerInfoWindow?: AgmInfoWindow;

    @Output()
    public onSwitchToTableView: EventEmitter<void> = new EventEmitter();

    constructor(private readonly cd: ChangeDetectorRef,
                private readonly http: HttpClient) {
    }

    public ngOnInit(): void {
        this.http.get<Station[]>('https://api.air-jarvis.ecargo.io/stations')
            .subscribe((res) => this.stations = res);
        this.http.get<LiveFlight[]>('https://api.air-jarvis.ecargo.io/flights')
            .subscribe((res) => this.flights = res);
    }

    @HostListener('window:resize')
    public onResize() {
        this.cd.detectChanges();
    }

    public mapReady(map: google.maps.Map): void {
        map.setOptions({
            streetViewControl: false
        });
    }

    public onFlightMarkerClick(infoWindow: AgmInfoWindow, flightNumber: string): void {
        this.previousMarkerInfoWindow?.close();
        this.previousMarkerInfoWindow = infoWindow;
        this.flightMarkerBookings = [];
        this.http.get<Booking[]>(`https://api.air-jarvis.ecargo.io/airwaybills/flight/${flightNumber}`)
            .subscribe(bookings => {
                this.flightMarkerBookings = bookings;
            });
    }
}
