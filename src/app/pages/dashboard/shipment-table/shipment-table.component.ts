import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LiveFlight} from "../../../shared/model/live-flight.model";
import {HttpClient} from "@angular/common/http";
import * as moment from "moment";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {RebookingWizardComponent} from "../../../shared/components/rebooking-wizard/rebooking-wizard.component";
import {Booking} from "../../../shared/model/booking.model";

interface TableRow extends LiveFlight {
    departureDate: string;
    departureTime: string;
    arrivalTime: string;
    timeLeftFmt: string;
}

function mapFlightToRow(flight: LiveFlight): TableRow {
    return {
        ...flight,
        departureDate: moment(flight.departureDateTime).format('DDMMM'),
        departureTime: moment(flight.departureDateTime).format('HHmm'),
        arrivalTime: moment(flight.arrivalDateTime).format('HHmm'),
        timeLeftFmt: Math.ceil(flight.timeLeft / 60) + 'h ' + flight.timeLeft % 60 + 'm',
    }
}

@Component({
    selector: 'app-shipment-table',
    templateUrl: './shipment-table.component.html',
    styleUrls: ['./shipment-table.component.scss']
})
export class ShipmentTableComponent implements OnInit {
    public flights?: TableRow[];
    public expandedRow?: TableRow;
    public expandedRowBookings?: Booking[];
    @Output()
    public onSwitchToMapView: EventEmitter<void> = new EventEmitter();

    constructor(
        private readonly http: HttpClient,
        private readonly modalService: NgbModal
    ) {
    }

    public ngOnInit(): void {
        this.http.get<LiveFlight[]>('https://api.air-jarvis.ecargo.io/flights')
            .subscribe((res) => this.flights = res.map(mapFlightToRow));
    }

    public rebookFlight(row: TableRow, booking?: Booking): void {
        const ref = this.modalService.open(RebookingWizardComponent);
        ref.componentInstance.flight = row;
        ref.componentInstance.booking = booking;
    }

    public expandRow(row: TableRow): void {
        this.expandedRow = row;
        this.expandedRowBookings = undefined;
        this.http.get<Booking[]>(`https://api.air-jarvis.ecargo.io/airwaybills/flight/${row.flightNumber}`)
            .subscribe(bookings => this.expandedRowBookings = bookings);
    }

    public closeRow(): void {
        this.expandedRow = undefined;
        this.expandedRowBookings = undefined;
    }
}
