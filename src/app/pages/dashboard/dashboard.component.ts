import {Component} from '@angular/core';

@Component({
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
    public mode: 'table' | 'map' = 'map';
}
