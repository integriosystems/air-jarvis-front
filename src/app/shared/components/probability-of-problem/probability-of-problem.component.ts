import {Component, Input} from '@angular/core';
import {ProbabilityOfProblem} from "../../model/live-flight.model";

@Component({
    selector: 'app-probability-of-problem',
    templateUrl: './probability-of-problem.component.html',
    styleUrls: ['./probability-of-problem.component.scss']
})
export class ProbabilityOfProblemComponent {
    public _value?: ProbabilityOfProblem;
    public percent: string = '7%';
    public color: '#A8DADC' | '#E6A339' | '#E63946' = '#A8DADC';
    public message: string = '';

    constructor() {
    }

    @Input()
    public set value(value: ProbabilityOfProblem) {
        this._value = value;
        if (value.value < 10)
            this.color = '#A8DADC';
        else if (value.value < 50)
            this.color = '#E6A339';
        else
            this.color = '#E63946';
        this.percent = value.value.toFixed(0) + '%';
        this.message = `Flight may be ${value.event === 'CANCELLED' ? 'cancelled' : 'delayed'}`
    }
}
