import {Component, OnInit} from '@angular/core';
import {LiveFlight} from "../../model/live-flight.model";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Booking} from "../../model/booking.model";
import {HttpClient} from "@angular/common/http";
import {SelectionChangedEvent} from "./rebooking-list/rebooking-list.component";

@Component({
    templateUrl: './rebooking-wizard.component.html',
    styleUrls: ['./rebooking-wizard.component.scss']
})
export class RebookingWizardComponent implements OnInit {
    public step: 'bookings' | 'flight' | 'confirmation' = "bookings";
    public flight?: LiveFlight;
    public bookings?: Booking[];
    public selectedBookings: Booking[] = [];

    constructor(
        private readonly http: HttpClient,
        public readonly activeModal: NgbActiveModal
    ) {
    }

    public set booking(value: Booking | undefined) {
        if (value)
            this.bookings = [value];
    }

    public ngOnInit(): void {
        if (this.bookings) {
            this.selectedBookings = [...this.bookings];
            this.step = 'flight';
        } else {
            this.http.get<Booking[]>(`https://api.air-jarvis.ecargo.io/airwaybills/flight/${this.flight?.flightNumber}`)
                .subscribe(bookings => this.bookings = bookings);
        }
    }

    public selectionChanged(event: SelectionChangedEvent) {
        if (event.selected)
            this.selectedBookings = [...this.selectedBookings, event.booking];
        else
            this.selectedBookings = this.selectedBookings.filter(b => b.awbNumber !== event.booking.awbNumber);
    }
}
