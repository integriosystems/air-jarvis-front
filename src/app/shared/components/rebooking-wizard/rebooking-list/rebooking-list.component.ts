import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Booking} from '../../../model/booking.model';

@Component({
    selector: 'app-rebooking-list',
    templateUrl: './rebooking-list.component.html',
    styleUrls: ['./rebooking-list.component.scss']
})
export class RebookingListComponent implements OnInit {
    @Input()
    public bookings?: Booking[];
    @Output()
    public selectionChanged: EventEmitter<SelectionChangedEvent> = new EventEmitter();

    constructor() {
    }

    public ngOnInit(): void {
    }
}

export interface SelectionChangedEvent {
    booking: Booking;
    selected: boolean;
}
