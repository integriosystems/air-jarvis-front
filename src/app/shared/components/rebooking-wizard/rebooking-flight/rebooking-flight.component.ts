import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ISODateTime} from "../../../model/types";
import * as moment from 'moment';

/*{
    "airline": {"iataCode": "AC", "icaoCode": "ACA", "name": "Air Canada"},
    "arrival": {
        "actualRunway": null,
        "actualTime": null,
        "baggage": null,
        "delay": null,
        "estimatedRunway": null,
        "estimatedTime": "2020-09-12T16:01:00.000+00:00",
        "gate": "D41",
        "iataCode": "YYZ",
        "icaoCode": "CYYZ",
        "scheduledTime": "2020-09-12T16:15:00.000+00:00",
        "terminal": "1"
    },
    "flight": {"iataNumber": "AC108", "icaoNumber": "ACA108", "number": "108"},
    "departure": {
        "actualRunway": "2020-09-12T09:17:00.000+00:00",
        "actualTime": "2020-09-12T09:17:00.000+00:00",
        "baggage": null,
        "delay": "17",
        "estimatedRunway": "2020-09-12T09:17:00.000+00:00",
        "estimatedTime": "2020-09-12T09:18:00.000+00:00",
        "gate": "C51",
        "iataCode": "YVR",
        "icaoCode": "CYVR",
        "scheduledTime": "2020-09-12T09:00:00.000+00:00",
        "terminal": "D"
    },
    "status": "active",
    "type": "departure"
}*/

interface AvailableFlightResponse {
    airline: { iataCode: string };
    flight: { iataNumber: string };
    arrival: { scheduledTime: ISODateTime };
    departure: { scheduledTime: ISODateTime };
    timeLeft: number;
}

interface AvailableFlight extends AvailableFlightResponse {
    arrivalTime: string;
    departureTime: string;
}

function mapAvailableFlight(flight: AvailableFlightResponse): AvailableFlight {
    return {
        ...flight,
        arrivalTime: moment(flight.arrival.scheduledTime).format('HHmm'),
        departureTime: moment(flight.departure.scheduledTime).format('HHmm')
    }
}

@Component({
    selector: 'app-rebooking-flight',
    templateUrl: './rebooking-flight.component.html',
    styleUrls: ['./rebooking-flight.component.scss']
})
export class RebookingFlightComponent implements OnInit, OnChanges {
    public flights?: AvailableFlight[];
    @Input()
    public origin?: string;
    @Input()
    public destination?: string;
    @Output()
    public rebookingComplete: EventEmitter<void> = new EventEmitter();

    constructor(
        private readonly http: HttpClient,
    ) {
    }

    public ngOnInit(): void {
        this.ngOnChanges();
    }

    public ngOnChanges(): void {
        if (this.origin && this.destination)
            this.http.get <AvailableFlightResponse[]>(`https://api.air-jarvis.ecargo.io/airportTimeTable/alternateFlights?origin=${this.origin}&destination=${this.destination}`)
                .subscribe((res) => this.flights = res.map(mapAvailableFlight));
    }
}
