import {Component, Input, OnChanges} from '@angular/core';

@Component({
    selector: 'app-time-left',
    templateUrl: './time-left.component.html',
    styleUrls: ['./time-left.component.scss']
})
export class TimeLeftComponent implements OnChanges {
    @Input()
    public value: number = 0;
    @Input()
    public max: number = 24 * 60;
    public color: 'color-0' | 'color-33' | 'color-66' = 'color-0';

    public ngOnChanges(): void {
        const percent = (this.max - this.value) / this.max * 100;
        if (percent < 33)
            this.color = 'color-0';
        else if (percent < 66)
            this.color = 'color-33';
        else
            this.color = 'color-66';
    }
}
