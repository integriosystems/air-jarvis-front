import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-info-box',
    templateUrl: './info-box.component.html',
    styleUrls: ['./info-box.component.scss']
})
export class InfoBoxComponent {
    @Input()
    public header?: string;
    @Input()
    public value?: number;
    @Input()
    public color?: '1' | '2';
    @Input()
    public link?: string;
}
