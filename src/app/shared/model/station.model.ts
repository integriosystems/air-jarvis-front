import {GeoLocation} from "./geo-location.model";

export interface Station extends GeoLocation {
    name: string;
}
