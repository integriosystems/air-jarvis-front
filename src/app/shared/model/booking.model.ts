export interface Booking {
    awbNumber: string;
    pieces: number;
    weight: number;
    status: string;
    routeStations: string[];
}
