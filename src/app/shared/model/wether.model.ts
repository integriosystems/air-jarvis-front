export interface Weather {
    temperature: number;
    windSpeed: number;
    pressure: number;
    icon: string;
}
