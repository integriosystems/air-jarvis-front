import {ISODateTime} from "./types";
import {GeoLocation} from "./geo-location.model";
import {Weather} from "./wether.model";

export interface ProbabilityOfProblem {
    value: number;
    event: "DELAYED" | "CANCELLED"
}

export interface LiveFlight extends GeoLocation {
    flightNumber: string;
    departureDateTime: ISODateTime;
    arrivalDateTime: ISODateTime;
    status: 'SCHEDULED' | 'DEPARTED' | 'CANCELLED';
    secondsBehindSchedule: number;
    origin: string;
    destination: string;
    flightDurationMinutes: number;
    probabilityOfProblem: ProbabilityOfProblem;
    timeLeft: number;
    weatherAtOrigin: Weather;
    weatherAtDestination: Weather;
}
