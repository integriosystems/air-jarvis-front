import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {routes} from './routes';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AgmCoreModule} from "@agm/core";
import {ShipmentTableComponent} from './pages/dashboard/shipment-table/shipment-table.component';
import {ShipmentMapComponent} from './pages/dashboard/shipment-map/shipment-map.component';
import {InfoBoxComponent} from './shared/components/info-box/info-box.component';
import {StatisticsComponent} from './pages/statistics/statistics.component';
import {ChartsModule} from "ng2-charts";
import {CancellationsComponent} from './pages/cancellations/cancellations.component';
import {RebookingWizardComponent} from './shared/components/rebooking-wizard/rebooking-wizard.component';
import {RebookingListComponent} from './shared/components/rebooking-wizard/rebooking-list/rebooking-list.component';
import {RebookingFlightComponent} from './shared/components/rebooking-wizard/rebooking-flight/rebooking-flight.component';
import {CommonModule} from "@angular/common";
import { ProbabilityOfProblemComponent } from './shared/components/probability-of-problem/probability-of-problem.component';
import { TimeLeftComponent } from './shared/components/time-left/time-left.component';
import { RebookingConfirmationComponent } from './shared/components/rebooking-wizard/rebooking-confirmation/rebooking-confirmation.component';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        ShipmentTableComponent,
        ShipmentMapComponent,
        InfoBoxComponent,
        StatisticsComponent,
        CancellationsComponent,
        RebookingWizardComponent,
        RebookingListComponent,
        RebookingFlightComponent,
        ProbabilityOfProblemComponent,
        TimeLeftComponent,
        RebookingConfirmationComponent,
    ],
    imports: [
        RouterModule.forRoot(
            routes
        ),
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NgbModule,
        ChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyASHPBRv6ATb90NIHI8VtdfdOlQfcDYCRw'
        })
    ],
    entryComponents: [RebookingWizardComponent],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
