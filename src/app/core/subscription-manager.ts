import {Observable, Subscription} from 'rxjs';

export class SubscriptionManager {
    private subscriptions: Subscription[] = [];
    private intervals: number[] = [];
    private timeouts: NodeJS.Timeout[] = [];

    public interval(handler: Function, interval: number) {
        this.intervals.push(setInterval(handler, interval));
    }

    public timeout(handler: Function, timeout: number) {
        const ref = setTimeout(() => {
            const idx = this.timeouts.indexOf(ref);
            this.timeouts = [
                ...this.timeouts.slice(0, idx),
                ...this.timeouts.slice(idx + 1)
            ];
            handler();
        }, timeout);
        this.timeouts.push(ref);
    }

    public subscribe<T>(observable: Observable<T>, next: (value: T) => void): void {
        this.subscriptions.push(observable.subscribe(next));
    }

    public cancelAll(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
        this.subscriptions = [];
        this.intervals.forEach(i => clearInterval(i));
        this.intervals = [];
        this.timeouts.forEach(i => clearTimeout(i));
        this.timeouts = [];
    }
}
