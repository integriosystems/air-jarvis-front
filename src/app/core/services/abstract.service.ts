import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CustomHttpParameterCodec} from './custom-http-parameter-codec';

export class HttpParamsBuilder {
    private params = new HttpParams({encoder: CustomHttpParameterCodec.INSTANCE});

    public set(name: string, value?: any): HttpParamsBuilder {
        if (typeof value === 'undefined' || value === null) {
            return this;
        }
        if (typeof value !== 'string') {
            value = String(value);
        }
        this.params = this.params.set(name, value);
        return this;
    }

    public toHttpParams() {
        return this.params;
    }
}

export abstract class AbstractService {
    protected constructor(
        private readonly http: HttpClient,
        private readonly endpoint: string
    ) {
    }

    protected httpParamsBuilder(): HttpParamsBuilder {
        return new HttpParamsBuilder();
    }

    protected get<T>(url?: string, params?: HttpParams): Observable<T> {
        return this.http.get<T>(`${this.endpoint}${url ? '/' : ''}${url || ''}`, {params});
    }

    protected post<T>(url?: string, body?: Object): Observable<T> {
        if (body instanceof HttpParamsBuilder) {
            body = body.toHttpParams();
        }
        return this.http
            .post<T>(
                `${this.endpoint}${url ? '/' + url : ''}`,
                body
            );
    }

    protected postBlob(url?: string, body?: Object): Observable<Blob> {
        return this.http
            .post(
                `${this.endpoint}${url ? '/' + url : ''}`,
                body,
                {responseType: 'blob'}
            );
    }

    protected put<T>(body: Object): Observable<T> {
        return this.http
            .put<T>(
                `${this.endpoint}`,
                body
            );
    }

    protected patch<T>(body: Object, url?: string): Observable<T> {
        return this.http
            .patch<T>(
                `${this.endpoint}${url ? '/' + url : ''}`,
                body
            );
    }

    protected delete<T>(url: string): Observable<T> {
        return this.http.delete<T>(`${this.endpoint}/${url}`);
    }
}
