import {Routes} from '@angular/router';
import {DashboardComponent} from "./pages/dashboard/dashboard.component";
import {StatisticsComponent} from "./pages/statistics/statistics.component";
import {CancellationsComponent} from "./pages/cancellations/cancellations.component";

export const routes: Routes = [
    {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    {path: 'dashboard', component: DashboardComponent},
    {path: 'statistics', component: StatisticsComponent},
    {path: 'cancellations', component: CancellationsComponent}
];
